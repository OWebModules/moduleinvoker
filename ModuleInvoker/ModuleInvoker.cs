﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace InvokeOModulesMethod
{
    public class ModuleInvoker
    {

        public string InvokeOModuleFunction(string loginUrl, string functionUrl, object body, int timeout = 100000)
        {
            var webRequest1 = WebRequest.Create(loginUrl) as HttpWebRequest;
            webRequest1.CookieContainer = new CookieContainer();
            var response = (HttpWebResponse)webRequest1.GetResponse();

            var webRequest2 = WebRequest.Create(functionUrl) as HttpWebRequest;
            webRequest2.Timeout = timeout;
            webRequest2.CookieContainer = new CookieContainer();
            foreach (Cookie cookie in response.Cookies)
            {
                webRequest2.CookieContainer.Add(cookie);
            }

            webRequest2.Method = "POST";

            webRequest2.ContentType = "application/json";

            var message = Newtonsoft.Json.JsonConvert.SerializeObject(body);

            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] byte1 = encoding.GetBytes(message);

            webRequest2.ContentLength = byte1.Length;
            Stream writeStream = webRequest2.GetRequestStream();

            writeStream.Write(byte1, 0, byte1.Length);
            response = (HttpWebResponse)webRequest2.GetResponse();

            string result = "";
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }

            return result;
        }
    }
}
