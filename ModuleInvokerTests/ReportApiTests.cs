﻿using InvokeOModulesMethod;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvokeOModulesMethodTests
{
    [TestClass]
    public class ReportApiTests
    {
        [TestMethod]
        public void SyncReport()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                idReport = GlobalData.IdReport
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlSyncReport,
                message
                );
        }

        [TestMethod]
        public void GetData()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                idReport = GlobalData.IdReport
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetData,
                message
                );
        }
    }
}
