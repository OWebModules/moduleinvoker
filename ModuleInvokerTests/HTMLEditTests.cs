﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvokeOModulesMethod;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InvokeOModulesMethodTests
{
    [TestClass]
    public class HTMLEditTests
    {
        [TestMethod]
        public void GetHtmlContent()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                idReference = GlobalData.idHtmlDocReference
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetHTMLContent,
                message
                );

        }
    }
}
