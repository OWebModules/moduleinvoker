﻿using InvokeOModulesMethod;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvokeOModulesMethodTests
{
    [TestClass]
    public class AppointmentTests
    {
        [TestMethod]
        public void GetAppointments()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetAppointments,
                message
                );
        }
    }
}
