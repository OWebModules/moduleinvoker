﻿using InvokeOModulesMethod;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvokeOModulesMethodTests
{
    [TestClass]
    public class BillTests
    {
        [TestMethod]
        public void GetTransactionsLastYear()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                idMandant = GlobalData.IdBillMandant
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetTransactions,
                message
                );
        }

        [TestMethod]
        public void GetTransactionsDateRange()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                idMandant = GlobalData.IdBillMandant,
                start = DateTime.Now.AddDays(-80),
                end = DateTime.Now
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetTransactions,
                message
                );
        }

        [TestMethod]
        public void GetTransactionData()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                idFinancialTransaction = GlobalData.IdBillTransaction
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetTransactionData,
                message
                );
        }

        [TestMethod]
        public void SaveNewTransaction()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                TransactionItems = new List<dynamic>
                {
                    new
                    {
                        TransactionName = "Testtransaction 1",
                        Gross = true,
                        ToPay = 1.5d,
                        TransactionDate = DateTime.Now,
                        IdCurrency = "853991fe45ce4d7f8df0984ff9840cd3",
                        Amount = 1.0d,
                        IdUnit = "bcd3f4d32c3747c590b36f2ea40b0f6d",
                        IdContractor = "e5d81b4d29544d1dac0937bbfc3a69e3",
                        IdContractee = "dbc3024144a6455ab48cc0cf59ee3c73",
                        IdTaxRate = "fad74770776f44a8bf0ebeb6a070218e",
                        undoItem = new
                        {
                            IdTransaction = ""
                        }
                    }
                }
            };

            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetClipboardItems,
                message
                );


        }

    }
}
