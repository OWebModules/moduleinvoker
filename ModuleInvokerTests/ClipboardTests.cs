﻿using InvokeOModulesMethod;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvokeOModulesMethodTests
{
    [TestClass]
    public class ClipboardTests
    {
        [TestMethod]
        public void GetAllClipboardItems()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetClipboardItems,
                message
                );
        }

        [TestMethod]
        public void ClearClipboardItems()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlClearClipboardItems,
                message
                );
        }

        [TestMethod]
        public void AddClipboardItems()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                OItems = new []
                {
                    new
                    {
                        GUID = "7bd073f6ee02424faab7568e78a62407"
                    },
                    new
                    {
                        GUID = "44e1e6aa7c4845d19bc6cd237531b7bb"
                    }
                }
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlAddClipboardItems,
                message
                );
        }

    }
}
