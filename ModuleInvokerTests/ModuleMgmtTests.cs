﻿using InvokeOModulesMethod;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvokeOModulesMethodTests
{
    [TestClass]
    public class ModuleMgmtTests
    {
        [TestMethod]
        public void SendInterserviceMessage()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance,
                message = new
                {
                    ChannelId = GlobalData.ChannelIdSelectedObject,
                    OItems = new[] { new { GUID = GlobalData.OItemId1 } }
                }
            };
            moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlSendInterserviceMessage,
                message
                );

        }
    }
}
