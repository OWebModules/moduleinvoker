﻿using System;
using System.Collections.Generic;
using InvokeOModulesMethod;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace InvokeOModulesMethodTests
{
    [TestClass]
    public class OItemsTests
    {
        

        [TestMethod]
        public void GetAttributeTypes()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetAttributeTypes,
                message
                );

            
        }

        [TestMethod]
        public void GetRelationTypes()
        {
            var moduleInvoker = new ModuleInvoker();
            var message = new
            {
                idSession = GlobalData.IdSession,
                idInstance = GlobalData.IdInstance
            };
            var result = moduleInvoker.InvokeOModuleFunction(GlobalData.UrlLogin,
                GlobalData.UrlGetRelationTypes,
                message
                );


        }
    }
}
