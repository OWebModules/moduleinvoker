﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvokeOModulesMethodTests
{
    public static class GlobalData
    {
        public static string server => "www.omodules.de/OModules";
        public static string UrlLogin => $"http://{server}/Account/LoginForScripts?email=tassilok@gmx.de&password=Wilbo!9";
        public static string UrlGetAppointments => $"http://{server}/AppointmentApi/GetAppointments";
        public static string UrlSendInterserviceMessage => $"http://{server}/ModuleMgmtApi/SendInterServiceMessage";
        public static string UrlGetAttributeTypes => $"http://{server}/OItemListApi/GetAttributeTypes";
        public static string UrlGetRelationTypes => $"http://{server}/OItemListApi/GetRelationTypes";
        public static string UrlGetTransactions => $"http://{server}/BillApi/GetTransactions";
        public static string UrlGetTransactionData => $"http://{server}/BillApi/GetTransactionData";
        public static string UrlSaveTransactionItems => $"http://{server}/BillApi/SaveTransactionItems";
        public static string UrlGetClipboardItems => $"http://{server}/ClipboardApi/GetClipboardItems";
        public static string UrlClearClipboardItems => $"http://{server}/ClipboardApi/ClearClipboard";
        public static string UrlAddClipboardItems => $"http://{server}/ClipboardApi/AddClipboardItems";
        public static string UrlSyncReport => $"http://{server}/ReportApi/SyncReport";
        public static string UrlGetData => $"http://{server}/ReportApi/GetData";

        public static string UrlGetHTMLContent => "http://localhost/OModules/HtmlEditApi/GetHtmlContent";

        public static string IdInstance => "abc22ae";
        public static string IdSession => "abc22ae";

        public static string ChannelIdSelectedObject => "898a19746c2a4b3e91c60c2db369e8e5";


        public static string OItemId1 => "8e259f4676e047c08917e3b7f38f3983";

        public static string IdBillMandant => "dbc3024144a6455ab48cc0cf59ee3c73";
        public static string IdBillTransaction => "2812cc44130942d3bfbf8ab69236f47d";

        public static string IdReport => "d2b0cfe688754661a1ed445f94796b02";

        public static string OItemId1 => "e36622bd718e40119aa2008d9429332c";

        public static string idHtmlDocReference => "e3b94b2a20234dde98788eb148f6491b";

    }
}
